#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <elf.h>

void print_shellcode_x86_64(unsigned char *data);

int main(int argc, char *argv[])
{
    int fd;
    struct stat sb;
    unsigned char *data;
    unsigned char *text;

    if (argc != 2)
    {
        fprintf(stderr, "usage: %s <bin>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    if ((fd = open(argv[1], O_RDONLY)) == -1)
    {
        perror("open");
        exit(EXIT_FAILURE);
    }

    if ((fstat(fd, &sb)) == -1)
    {
        perror("fstat");
        exit(EXIT_FAILURE);
    }

    if ((data = (unsigned char *)mmap(0, sb.st_size, PROT_READ, MAP_SHARED, fd, 0)) == MAP_FAILED)
    {
        perror("mmap");
        exit(EXIT_FAILURE);
    }

    if ((strncmp(data, ELFMAG, 4)) != 0)
    {
        fprintf(stderr, "error: %s: Not an ELF file\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    if (data[EI_CLASS] == ELFCLASS64) print_shellcode_x86_64(data);

    else
    {
        fprintf(stderr, "error: %s: Unknown architecture\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    close(fd);

    return 0;
}

void print_shellcode_x86_64(unsigned char *data)
{
    Elf64_Ehdr *ehdr = (Elf64_Ehdr *)data;
    Elf64_Shdr *shdr = (Elf64_Shdr *)(data + ehdr->e_shoff);
    unsigned char *strtab = data + shdr[ehdr->e_shstrndx].sh_offset;
    unsigned char *pbyte;
    Elf64_Off offset = 0;
    uint64_t size = 0, n = 0;
    int i;

    for (i = 0; i < ehdr->e_shnum; i++)
    {
        if (!strcmp(strtab + shdr[i].sh_name, ".text"))
        {
            offset = shdr[i].sh_offset;
            size = shdr[i].sh_size;
            break;
        }
    }

    if (!offset && !size)
    {
        fprintf(stderr, "error: No \".text\" section\n");
        exit(EXIT_FAILURE);
    }

    pbyte = data + offset;

    printf("\"");

    while (n < size)
    {
        printf("\\x%.2x", *pbyte);

        pbyte++;
        n++;

        if (!(n % 15)) printf("\"\n\"");
    }

    if (n % 15) printf("\"");

    printf("\n");
}

