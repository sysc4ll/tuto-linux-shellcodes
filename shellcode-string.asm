section .text

global _start

_start:

xor ebx,ebx     ; on met ebx (32 bits) à 0, donc bl (8 bits) aussi
jmp getString   ; on jump sur getString

getStringReturn:
pop ecx         ; on pop dans ecx le haut de la stack, soit le ptr sur notre string "hello world#"
mov [ecx+11],bl ; on écrase le '#' par bl, donc 0

getString:
; le call va "pusher" sur la stack l'adresse de l'instruction qui suit, soit l'adresse de notre string "hello world#"
call getStringReturn
db "hello world#"

